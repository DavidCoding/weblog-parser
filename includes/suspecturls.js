// These are substrings which if discovered in the URL should be flagged for closer investigation, as it might indicate bad intent on the part of the requester
// although it could also be legitimate traffic (a recognised IP accessing an admin page) - hence we consider it suspicious, not immediately assuming bad intent
// You could also include where people are searching for a software package you don't use
// For example - if your website is not in WordPress/Drupal/Joomla/etc, then any attempts to access associated files would be suspect in your environment.

// All URLs are swapped to lowercase during processing for easier matching, where particularly relevant the original case will be included in comments

var susURLs = [
    "1index","1ogin","1=1",
    "2index",
    "3index","3x=3x",
    "admin","alfa","alfindex","alf4","am*guaw8.rydgz-tyf","author=",
    "backoffice","backup","bala=up","beence","biif","boom.php","bypass","byp4ss","b1a3k","b374k",
    "cjfuns","concat","config","credentials","c99",
    "d3wl7","daksldlkdsadas","database","db-safe-mode","dbweb","db.php","deadcode","defaul1","defau1t","defau11","delpaths","dkiz","download","dump",
    "elfinder","evil.php","extractvalue",
    "gank","gaza","gecko","githubusercontent","ghost=send","gutsevich",
	"h4ck","h4x","hack","haders","hanna1337","hunter",
	/*iR7SzrsOUEP*/
    "install","information_schema","instabuilder","ir7Szrsouep","isko",
    "legion.php","localhost","lock0360","lock360","login","lufi",
    "marijuana","moduless",
	/* "NmRtJOUjAdutReQj/scRjKUhleBpzmTyO", */
    "nmrtjoujadutreqj/scrjkuhlebpzmtyo","null",
    "old-index","old-site","olux","owa",
    "passwd","password","phpinfo","phpmanager","phpmyadmin","phpshells","pma","priv8","pr1v",
    "rxr","r57","rdweb",
    "s_e","s_ne","select","sfilename","shell20211028","signin","smevk","sqlmanager","sqlweb","symbpass","syml1nk","symlink","sym403",
    "test=hello","tf2rghf.jpg","t2rghf.jpg",
	"ueditor","umvusxn4hvg3bzrf","upload","user-new",
    "vuln","v3n0m",
    "waitfor","webdb","webroot","webr00t","websql","whm-crack","wlwmanifest","writetofile","wrong_datlib","wso.php","wso112233","wsoyanzorng","wsoshell","wwwroot",
    "xleet","xlettt","xltravat","xl2023","xmlrpc","xmrlpc","xploit","xzourt",
	"yanz",
    "\%20","\%2a","\%2f",
    // Including some file extensions here - again common sense should be applied. If your website offers zip/rar files for download then it could be legitimate traffic
    ".7z",".bak",".bck",".bz",".bz2",".env",".gz",".rar",".sql",".tar",".tgz",".zip"
];
module.exports = {susURLs}