// Arrays for holding IPs flagged

// Will be used to build notes column
var InternalIPs = [];

var InternalIPsV6 = [];

var SuspectIPs = [];

var SuspectIPsV6 = [];

var badIPs = [];

var badIPsV6 = [];

module.exports = {InternalIPs,SuspectIPs,badIPs,InternalIPsV6,SuspectIPsV6,badIPsV6}