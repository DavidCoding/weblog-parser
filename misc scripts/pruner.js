// Script to strip out non-bot traffic based on scanning for common identifiers

// Readline

const fs = require('fs');
const readline = require('readline');
var Excel = require('exceljs');

// Try using yargs to handle combinations of parameters
var argv = require('yargs/yargs')(process.argv.slice(2)).argv;

if (argv.log) {
	var log = argv.log;
	switch(argv.log) {
		case 'apache':
			var rl = readline.createInterface({
				input: fs.createReadStream('apache.log'),
				output: process.stdout,
				terminal: false
			});
			break;
		default:
			var rl = readline.createInterface({
				input: fs.createReadStream('IIS.log'),
				output: process.stdout,
				terminal: false
			});
			break;
	}
} else {
	var rl = readline.createInterface({
		input: fs.createReadStream('IIS.log'),
		output: process.stdout,
		terminal: false
	});
	var log = 'IIS';
}

// Use general check/get ip functions
var functions = require('../includes/logparse-process.js');
var checkip = functions.checkip;
var getip = functions.getip;

/* var Recs = []; */

var botagents = require('../includes/bots.js').botagents;

// Set up workbook and worksheet
// Moved here, originally handled at end of log file
var workbook = new Excel.Workbook();
var worksheet = workbook.addWorksheet("Bot Records");
var coldef = [];
coldef[0] = { header: "Record", key:"RECORD"};
if (argv.botip) {
	coldef[1] = { header: "Bot IP?", key:"BOTIP"};
}
worksheet.columns = coldef;
worksheet.getRow(1).font = { name: "Calibri", size: 11, bold: true};

rl.on('line', (string) => {
	// First things first, if we're excluding blocked/internal lets find out now
	if (argv.noblocked || argv.nointernal || argv.botip) {
		var IPAdd = getip(string,log);
		if (IPAdd != null) {
			var checkedip = checkip(IPAdd,' ');
			// Also use HTTP status - if 403 then the request was blocked whether or not the IP is
			if (argv.log == 'IIS') {
				var HTTPstart = string.search(/\s\d{3}(?=( (\d*)){5})/g);
				var HTTPstat = string.substring(HTTPstart + 1,HTTPstart + 4);
			} else {
				var HTTPstart = string.search(/\s\d{3}\s/g);
				var HTTPstat = string.substring(HTTPstart + 1,HTTPstart + 4);
			}
		}
	}
	// If we're excluding blocked/internal then below if will skip adding to array
	if ( (!argv.noblocked || argv.noblocked && checkedip != 'Blocked Address!' && HTTPstat != 403)
	&& (!argv.nointernal || argv.nointernal && checkedip != 'Internal Address!')) {
		// replace false positives, such as any files with 'bot' in the name
		// robots.txt can be excluded as well, although odds are if something is accessing it then it's a bot?
		//string = string.replace(/robots.txt/gi,"");
		string = string.replace(/bottom/gi,"");
		string = string.replace(/roboto/gi,"");
		// convert string to lowercase to make matching easier
		var tempstring = string.toLowerCase();
		// now check for the term 'bot', 'crawler', 'spider'
		if (tempstring.indexOf('bot') != -1 
		|| tempstring.indexOf('crawl') != -1
		|| tempstring.indexOf('spider') != -1) 
		 {
			var unknown = checkbot(tempstring);
			if (unknown == '') {
				/* Recs.push(string); */
				const rowdef = [];
				rowdef[0] = string;
				if (argv.botip && (checkedip == 'Bot Address!' || checkedip == 'Bot Subnet!')) {
					rowdef[1] = 'Y';
				}
				worksheet.addRow(rowdef);
			}
		}
	}
})
.on('close', function() {
	// Previously we were doing all the Excel functions here, but there's no particular need to
	// Saves us looping through the array just to output the records to the spreadsheet

	workbook.xlsx.writeFile("pruned.xlsx");
});

// stripdown version of normal checkbot function - just check if the agent is already known

function checkbot(string) {
	var tempstring = string.toLowerCase();
	for (key in botagents){
		var botagent = key;
		if (tempstring.indexOf(botagent) != -1) {
			return(botagents[key].bot + " address!");
		}
	}
	return("");
}