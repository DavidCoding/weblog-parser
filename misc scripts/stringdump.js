// Script to output records including specified substring

const fs = require('fs');
const readline = require('readline');
var Excel = require('exceljs');

var argv = require('yargs/yargs')(process.argv.slice(2)).argv;

if (argv.log) {
	switch(argv.log.toUpperCase()) {
		case 'IIS':
			var rl = readline.createInterface({
				input: fs.createReadStream('IIS.log'),
				output: process.stdout,
				terminal: false
			});
			break;
		case 'APACHE':
			var rl = readline.createInterface({
				input: fs.createReadStream('apache.log'),
				output: process.stdout,
				terminal: false
			});
			break;
		case 'EVERYTHING':
			var rl = readline.createInterface({
				input: fs.createReadStream('everything.php'),
				output: process.stdout,
				terminal: false
			});
			break;
		case 'JOOMLA':
			var rl = readline.createInterface({
				input: fs.createReadStream('error.php'),
				output: process.stdout,
				terminal: false
			});
			break;		
		default:
			console.log('invalid log type passed - cannot process')
			process.exit(1);
	} 
} else {
	console.log('no log type passed so cannot process')
	process.exit(1);
}

if (argv.match == null) {
	console.log('no string provided so cannot process');
	process.exit(1);
}

if (argv.nocase) {
	argv.match = argv.match.toLowerCase();
	if (argv.exclude) {
		argv.exclude = argv.exclude.toLowerCase();
	}
}

// Set up workbook and worksheet
// Moved here, originally handled at end of log file
var workbook = new Excel.Workbook();
var worksheet = workbook.addWorksheet("Records");
var coldef = [];
coldef[0] = { header: "Record", key:"RECORD"};
worksheet.columns = coldef;
worksheet.getRow(1).font = { name: "Calibri", size: 11, bold: true};

rl.on('line', (string) => {
	if (argv.nocase) {
		tempstring = string.toLowerCase();
	}
	// test whether requested substring exists in line, if so then push for output.
	if (string.indexOf(argv.match) != -1 || (argv.nocase && tempstring.indexOf(argv.match) != -1)) {
		// new optional exclude substring can be used if, for example, you want 'admin' but not 'administrator' to be output
		if (argv.exclude == null || argv.exclude && (string.indexOf(argv.exclude) == -1 && (!argv.nocase || 
		argv.nocase && tempstring.indexOf(argv.exclude) == -1))) {
			/* Recs.push(string); */
			const rowdef = [];
			rowdef[1] = string;
			worksheet.addRow(rowdef);
		}
	}
})
.on('close', function() {

	// Previously we were doing all the Excel functions here, but there's no particular need to
	// Saves us looping through the array just to output the records to the spreadsheet

	workbook.xlsx.writeFile("stringdump.xlsx");
});