// Script to determine last time a web crawler was seen based on user agent

// Note that this means if a crawler can be triggered from multiple locations or multiple users
// then those will be grouped together.

// Readline

const fs = require('fs');
const readline = require('readline');
var Excel = require('exceljs');

var agents = [];
var FirstDate = [];
var LastDate = [];

// Try using yargs to handle combinations of parameters
var argv = require('yargs/yargs')(process.argv.slice(2)).argv;

if (argv.log) {
	var log = argv.log;
	switch(argv.log) {
		case 'apache':
			var rl = readline.createInterface({
				input: fs.createReadStream('apache.log'),
				output: process.stdout,
				terminal: false
			});
			break;
		default:
			var rl = readline.createInterface({
				input: fs.createReadStream('IIS.log'),
				output: process.stdout,
				terminal: false
			});
			break;
	}
} else {
	var rl = readline.createInterface({
		input: fs.createReadStream('IIS.log'),
		output: process.stdout,
		terminal: false
	});
	var log = 'IIS'
}

// Use general check/get ip functions
var functions = require('../includes/logparse-process.js');
var checkip = functions.checkip;
var getip = functions.getip;

var botagents = require('../includes/bots.js').botagents;

rl.on('line', (string) => {
	// First things first, if we're excluding blocked/internal lets find out now
	if (argv.noblocked) {
		var IPAdd = getip(string,log);
		if (IPAdd != null) {
			var checkedip = checkip(IPAdd,' ');
			// Also use HTTP status - if 403 then the request was blocked whether or not the IP is
			if (argv.log == 'IIS') {
				var HTTPstart = string.search(/\s\d{3}(?=( (\d*)){5})/g);
				var HTTPstat = string.substring(HTTPstart + 1,HTTPstart + 4);
			} else {
				var HTTPstart = string.search(/\s\d{3}\s/g);
				var HTTPstat = string.substring(HTTPstart + 1,HTTPstart + 4);
			}
		}
	}
	// Exclude lines beginning with # as they're comments
	if (string.indexOf('#') !== 0 && (!argv.noblocked || 
		(argv.noblocked && IPAdd != null && checkedip.indexOf('Blocked') == -1 && HTTPstat != '403'))) {
		// checkbot used to provide internal notes
		var checkedbot = checkbot(string);
		if (argv.log == 'apache') {
			var DateStart = string.indexOf("[") + 1;
			var DateEnd = string.indexOf("]");
			var DateCleanup = string.substring(DateStart,DateEnd);
			// Remove the space before the timezone adjustment
			var DateCleanup = DateCleanup.replace(" ","");
			// Now add a space between date and time
			var DateCleanup = DateCleanup.replace(":"," ");
			var DateNew = new Date(DateCleanup);
		} else {
			var DateNew = new Date(string.substring(0,19));
		}
		if (agents.includes(checkedbot)) { 
			if (DateNew > LastDate[agents.indexOf(checkedbot)]) {
				LastDate[agents.indexOf(checkedbot)] = DateNew;
			} else if (DateNew < FirstDate[agents.indexOf(checkedbot)]) {
				FirstDate[agents.indexOf(checkedbot)] = DateNew;
			}
		} else if (checkedbot != '') {
			agents.push(checkedbot)
			FirstDate.push(DateNew);
			LastDate.push(DateNew);
		}
	}
})

.on('close', function() {
	// Set up workbook and worksheet
	var workbook = new Excel.Workbook();
	var worksheet = workbook.addWorksheet("Bot Records");
	var coldef = [];
	coldef[0] = { header: "BotAgent", key:"BOTAGENT"};
	coldef[1] = { header: "First Seen", key:"FIRST", width: 11};
	coldef[2] = { header: "Last Seen", key:"LAST", width: 11};
	worksheet.columns = coldef;
	worksheet.getRow(1).font = { name: "Calibri", size: 11, bold: true};
	// Loop array of unique records
	var counter = 0;
	agents.forEach(function(element){
		const rowdef = [];
		rowdef[0] = element;
		rowdef[1] = FirstDate[counter];
		rowdef[2] = LastDate[counter];
		worksheet.addRow(rowdef);
		counter++;
	})
	workbook.xlsx.writeFile("crawlers.xlsx");
});

// stripdown version of normal checkbot function - just check if the agent is already known

function checkbot(string) {
	var tempstring = string.toLowerCase();
	for (key in botagents){
		var botagent = key;
		if (tempstring.indexOf(botagent) != -1) {
			return(botagents[key].bot + " address!");
		}
	}	
	return("");
}