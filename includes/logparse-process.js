
// botIPs array moved to external javascript file
var botIPs = require('./bots.js').botIPs;
var botIPsV6 = require('./bots.js').botIPsV6;
var botagents = require('./bots.js').botagents;
var botblocked = require('./bots.js').botblocked;
var susURLs = require('./suspecturls.js').susURLs;
var InternalIPs = require('./iparrays.js').InternalIPs;
var SuspectIPs = require('./iparrays.js').SuspectIPs;
var badIPs = require('./iparrays.js').badIPs;
var InternalIPsV6 = require('./iparrays.js').InternalIPsV6;
var SuspectIPsV6 = require('./iparrays.js').SuspectIPsV6;
var badIPsV6 = require('./iparrays.js').badIPsV6;

// Using Netmask to build array of blocked subnets
var Netmask = require('netmask').Netmask;

var blocks = [/*new Netmask('192.168.1.1/24') */
];

//buildline - accepts string from readline and builds for output
function buildline(string,logtype,modetype){
	// Extract IP address
	var IPAdd = getip(string,logtype);
	switch (logtype) {
		case 'IIS':
			// Start of URL will be first instance of a forward slash
			// End of URL will be before the port number, normally 443/80
			// If URL is under a certain length then IIS appears to add a - before the port
			// So test for both scenarios
			// 2021/12/23 - Rework checking for - to make a more accurate match if there are multiple instances
			// 2022/01/11 - Add - to end of initial 443/80 check to handle odd chance of it appearing in bytes/time taken/etc
			var urlend = string.indexOf(' 443 -');
			if (urlend == -1) {
				var urlend = string.indexOf(' 80 -');
			}
			if (string.lastIndexOf(' - 443',urlend) !== -1 || string.lastIndexOf(' - 80',urlend) !== -1) {
			/* if (string.lastIndexOf(' - ',urlend) !== -1) { */
				/* var urlend = string.indexOf(' - '); */
				var urlend = string.lastIndexOf(' - ',urlend);
			}
			var urlreq = string.substring(string.indexOf('/'),urlend);
			// IIS not displaying ? in url request so put it back in
			urlreq = urlreq.replace(" ","?");
			// Get HTTP status using Regex to find 3 digits followed by a series of 
			// 5 spaces seperated by any number of digits
			// 200 0 0 15669 344 546
			// 2021/09/16 - add a preceding white character to ensure we don't pick up a port
			// on the website address
			var HTTPstart = string.search(/\s\d{3}(?=( (\d*)){5})/g);
			var HTTPstat = string.substring(HTTPstart + 1,HTTPstart + 4);
			// Build CurrentLine from the various elements we've picked up
			// If a mode has been specified, use that
			switch (modetype) {
				case 'summstat':
					CurrentLine = (IPAdd + ' ' + HTTPstat);
					break;
				case 'summurl':
					CurrentLine = (IPAdd + ' ' + urlreq);
					break;
				case 'summip':
					CurrentLine = (IPAdd);
					break;
				case 'urlonly':
					CurrentLine = urlreq;
					break;
				case 'urlstat':
					CurrentLine = urlreq + ' ' + HTTPstat;
					break;
				default:
					CurrentLine = (IPAdd + ' ' + urlreq + ' ' + HTTPstat);
					break;
			}
			break;
		case 'apache':
			// URL will be between two quote marks (") with a GET/POST prefix and HTTP/version suffix
			if (string.indexOf('"GET') != -1) {
				var urlreq = string.substring(string.indexOf('"GET') + 5,string.indexOf(' HTTP/'))
			} else if (string.indexOf('"POST') != -1) {
				var urlreq = string.substring(string.indexOf('"POST') + 6,string.indexOf(' HTTP/'))
			} else if (string.indexOf('"HEAD') != -1) {
				var urlreq = string.substring(string.indexOf('"HEAD') + 6,string.indexOf(' HTTP/'))
			} else {
				// If we got here there was no GET/POST so just take whatever was between the first two
				// quote marks in the string
				var start = string.indexOf('"') + 1;
				var urlreq = string.substring(string.indexOf('"') + 1,string.indexOf('"',start))	
			}
			// Get HTTP status using Regex to find 3 digits with a space either side
			var HTTPstart = string.search(/\s\d{3}\s/g);
			var HTTPstat = string.substring(HTTPstart + 1,HTTPstart + 4);
			// Build CurrentLine from the various elements we've picked up
			// If a mode has been specified, use that
			switch (modetype) {
				case 'summstat':
					CurrentLine = (IPAdd + ' ' + HTTPstat);
					break;
				case 'summurl':
					CurrentLine = (IPAdd + ' ' + urlreq);
					break;
				case 'summip':
					CurrentLine = (IPAdd);
					break;
				case 'urlonly':
					CurrentLine = urlreq;
					break;
				case 'urlstat':
					CurrentLine = urlreq + ' ' + HTTPstat;
					break;
				default:
					CurrentLine = (IPAdd + ' ' + urlreq + ' ' + HTTPstat);
					break;
			}
			break;
		// else use Joomla logic
		default:
			// Handle older version of Joomla logging
			if (string.indexOf('Joomla FAILURE') !== -1) {
				// Add 43 characters to length - gets us to error message
				nextpos = IPAdd.length + 43;
			} else {
				// Add 46 characters to length - gets us to error message
				nextpos = IPAdd.length + 46;
			}
			CurrentLine = (IPAdd + ' ' + string.substring(nextpos));
		break;
	}
	// urlonly will still use IP here as it's a sense check against null lines
	if (IPAdd != '' && IPAdd != ' ') {
		return(CurrentLine);
	} else {
		return("");
	}
}

//buildcols - build column array and return
function buildcols(logtype,modetype){
	var coldef = [];
	var counter = 0;
	// If in IIS mode then include column for HTTP status code
	if (logtype == 'IIS' || logtype == 'apache') {
		// Build column headings depending on mode, all require IP address
		// Above now true for all except 'urlonly' mode
		if (modetype != 'urlonly' && modetype != 'urlstat') {
			coldef[counter] = { header: "IP Address", key:"IPADD", width: 15};
			counter++;
		}
		switch (modetype) {
			case 'summstat':
				coldef[counter] = { header: "HTTP Status", key:"HTTPSTAT"};
				counter++;
				break;
			case 'summurl':
				coldef[counter] = { header: "Record", key:"RECORD"};
				counter++;
				break;
			case 'summip':
				break;
			case 'urlonly':
				coldef[counter] = { header: "Record", key:"RECORD"};
				counter++;
				break;
			default:
				coldef[counter] = { header: "Record", key:"RECORD"};
				counter++;
				coldef[counter] = { header: "HTTP Status", key:"HTTPSTAT"};
				counter++;
				break;
		}
		// All then have the final 4 columns the same
		coldef[counter] = { header: "Times Found", key:"COUNT"};
		counter++;
		coldef[counter] = { header: "First Found", key:"FIRST", width: 11};
		counter++;
		coldef[counter] = { header: "Last Found", key:"LAST", width: 11};
		counter++;
		coldef[counter] = { header: "Notes", key:"NOTES"};
	} else {
		coldef[counter] = { header: "IP Address", key:"IPADD", width: 15}
		counter++;
		coldef[counter] = { header: "Record", key:"RECORD"}
		counter++;
		coldef[counter] = { header: "Times Found", key:"COUNT"}
		counter++;
		coldef[counter] = { header: "First Found", key:"FIRST", width: 11}
		counter++;
		coldef[counter] = { header: "Last Found", key:"LAST", width: 11}
		counter++;
		coldef[counter] = { header: "Notes", key:"NOTES"}
	}
	return (coldef)
}

function checkip(IPaddress,bottype){
	switch (IPaddress.indexOf(':')) {
		case -1:
			// Allow a 3 part match i.e. 127.0.0.* - currently only for bots
			var subnet = IPaddress.substring(0,IPaddress.lastIndexOf('.') + 1)

			// First draft of subnet blocking logic - currently (web)app are calling checkip before excluding comment lines
			// this will be revisited later
			let loop = 0;
			while (blocks[loop]) {
				if (blocks[loop].contains(IPaddress)) {
					if (badIPs.includes(IPaddress) ) {
						return("Blocked address & subnet!");
					} else {
						return("Blocked Subnet!");
					}
				}
				loop++;
			}
			if (InternalIPs.includes(IPaddress) ) {
				return("Internal Address!");
			} else if (SuspectIPs.includes(IPaddress) ) {
				return("Monitored Address!");
			} else if (badIPs.includes(IPaddress) ) {
				return("Blocked Address!");
			} else if (botIPs.includes(IPaddress) && bottype != "agent" && bottype != "exclude") {
				return("Bot Address!");
			} else if (botIPs.includes(subnet) && botIPs[botIPs.indexOf(subnet)].substr(-1) == '.' 
						&& bottype != "agent" && bottype != "exclude"){
				return ("Bot Subnet!");
			} else {
				return("");
			}
			break;
		default:
			if (InternalIPsV6.includes(IPaddress) ) {
				return("Internal Address!");
			} else if (SuspectIPsV6.includes(IPaddress) ) {
				return("Monitored Address!");
			} else if (badIPsV6.includes(IPaddress) ) {
				return("Blocked Address!");
			} else if (botIPsV6.includes(IPaddress) && bottype != "agent" && bottype != "exclude") {
				return("Bot Address!");
			} else {
				return("");
			}
			break;
	}
};

//Check for various bot agents and see if we already have the IP address on record
function checkbot(string,IPAdd,bottype) {
	let i = 0;
	var tempstring = string.toLowerCase();
	var returnstring = "";
	/* while (botagents[i] && returnstring == "") {
		if (tempstring.indexOf(botagents[i]) != -1) { */
	for (key in botagents){
		var botagent = key;
		if (tempstring.indexOf(botagent) != -1) {
			if ((bottype != "agent" && bottype != "exclude")){
				var found = checkip(IPAdd);
				if (found == '') {
					/* returnstring = ("New " + botagents[i] + " address!"); */
					returnstring = ("New " + botagents[key].bot + " address!");
				} else {
					/* returnstring = (botagents[i] + " address!"); */
					returnstring = (botagents[key].bot + " address!");
				}
			} else {
				/* returnstring = (botagents[i] + " address!"); */
				returnstring = (botagents[key].bot + " address!");
			}
		}
		i++;
	}
	/* return(""); */
	// now also check the botblocked array to see if this bot has been told to take a hike
	if (returnstring == "") {
		return("");
	}
	i = 0;
	while (botblocked[i]) {
		if(tempstring.indexOf(botblocked[i]) != -1) {
			returnstring = returnstring + " bad bot!";
			return(returnstring)
		}
		i++;
	}
	return(returnstring);
}

function getip(string,log) {
	if (log == "IIS") {
		// Figure out where IP address is - search for 3 sets of digits
		// with a decimal inbetween, followed by a 4th set of digits.
		// Must also have 10 spaces preceeding it to not pick up host IP
		// 2021-12-21 Reduce from 10 spaces to 5 to cope with more IIS log formats
		// 2023-05-19 Add a space after the IP - catch when an IP is part of the query parameters of a URL
		// 2023-06-12 Add a space before the IP for much the same reason. Also means IPStart in substring needs a +1
		var IPStart = string.search(/ (\d{1,3}\.){3}\d{1,3} (?<=( (.*)){5})/g);
		// Test in case an odd record got here
		if (IPStart != -1) {
			var IPAdd = string.substring(IPStart+1,string.indexOf(' ',IPStart+1));
		}
	} else {
		// Joomla only has a single IP address in the record, so no need for
		// additional checks. Uses a tab rather than a space after the IP address.
		if (log == 'apache') {
			var IPStart = string.search(/(\d{1,3}\.){3}\d{1,3} /g);	
		} else {
			var IPStart = string.search(/(\d{1,3}\.){3}\d{1,3}	/g);
		}
		// taken from https://stackoverflow.com/questions/53497/regular-expression-that-matches-valid-ipv6-addresses on 2023/09/19
		var IPv6Start = string.search(/(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))/g)
		// Currently don't handle IPv6 addresses so skip
		if (IPv6Start != -1 && (IPv6Start < IPStart || IPStart == -1)) {
			IPStart = IPv6Start
		}
		// Test in case an odd record got here
		if (IPStart != -1) {
			// piggyback apache logic, it uses spaces not tabs however
			if (log == 'apache') {
				var IPAdd = string.substring(IPStart,string.indexOf(' ',IPStart));
			} else {
				var IPAdd = string.substring(IPStart,string.indexOf('	',IPStart));
			}
		}
	}
	return IPAdd;
}

// New function to handle checking inclusion (my head hurt having it in (web)app.js )
// Also splitting into individual if statements for now to make it easier to read
function checkinclude(bottype,blocked,internal,checkedbot,checkedip) {
	// excluding bots and checkbot found something
	if ((bottype == 'exclude' || bottype == "excludesus") && checkedbot != "") {
		return('N')
	}
	// excluding suspect bots and checkip found something
	// checkbot finding something should be caught above
	if ((bottype == 'excludesus') && (checkedip.indexOf('Bot') != -1)) {
		return('N')
	}
	// or we're only including bots
	if ((bottype == 'only') && checkedbot == '') {
		return('N')
	}
	// Excluding blocked IPs?
	if (blocked == 'N' && (checkedip == 'Blocked Address!'
	|| checkedip == 'Blocked Subnet!' || checkedip == 'Blocked address & subnet!')) {
		return('N')
	}
	// Only blocked IPs?
	if (blocked == 'O' && (checkedip != 'Blocked Address!'
		&& checkedip != 'Blocked Subnet!' && checkedip != 'Blocked address & subnet!')) {
		return('N')
	}
	// Excluding internal IPs?
	if (internal == 'N' && checkedip == 'Internal Address!') {
		return('N')
	}
	// Only blocked IPs?
	if (internal == 'O' && checkedip != 'Internal Address!') {
		return('N')
	}
	// otherwise lets return a Y
	return('Y')
}

// New function to handle checking exclusion based on images/js/css flags
function checkexclude(string,logtype,noimages,nojs,nocss,notemp,extarr) {
		// Below borrowed from buildline routine
		if (logtype == 'IIS') {
			// may move checkexclude within it once flags are added to commandline
			// but for now since this is web-only we'll do it standalone.
			// 2021/12/31 - Rework checking to match updated buildline logic
			var urlend = string.indexOf(' 443 ');
			if (urlend == -1) {
				var urlend = string.indexOf(' 80 ');
			}
			if (string.lastIndexOf(' - 443',urlend) !== -1 || string.lastIndexOf(' - 80',urlend) !== -1) {
			/* if (string.lastIndexOf(' - ',urlend) !== -1) { */
				/* var urlend = string.indexOf(' - '); */
				var urlend = string.lastIndexOf(' - ',urlend);
			}
			var urlreq = string.substring(string.indexOf('/'),urlend);
		} else {
			// URL will be between two quote marks (") with a GET/POST prefix and HTTP/version suffix
			if (string.indexOf('"GET') != -1) {
				var urlreq = string.substring(string.indexOf('"GET') + 5,string.indexOf(' HTTP/'))
			} else if (string.indexOf('"POST') != -1) {
				var urlreq = string.substring(string.indexOf('"POST') + 6,string.indexOf(' HTTP/'))
			} else {
				// If we got here there was no GET/POST so just take whatever was between the first two
				// quote marks in the string
				var urlreq = string.substring(string.indexOf('"'),string.indexOf('"',string.indexOf('"' + 1)))	
			}
		}
		var tempstring = urlreq.toLowerCase();
		if (tempstring.indexOf('.js') != -1 && nojs == 'Y') {
			return('Y');
		}
		if (tempstring.indexOf('.css') != -1 && nocss == 'Y') {
			return('Y');
		}
		if (( tempstring.indexOf('.png') != -1 || tempstring.indexOf('.gif') != -1 ||
		tempstring.indexOf('.jpg') != -1 || tempstring.indexOf('.jpeg') != -1 ||
		tempstring.indexOf('.svg') != -1  || tempstring.indexOf('.ico') != -1 || 
		tempstring.indexOf('.webp') != -1 || tempstring.indexOf('.apng') != -1 || 
		tempstring.indexOf('.avif') != -1 || tempstring.indexOf('.webp') != -1 || 
		tempstring.indexOf('.jfif') != -1 || tempstring.indexOf('.pjpeg') != -1 || 
		tempstring.indexOf('.pjp') != -1 || tempstring.indexOf('.tif') != -1 || 
		tempstring.indexOf('.bmp') != -1 )
		&& noimages == 'Y') {
			return('Y');
		}
		if (tempstring.indexOf('/cache/') != -1 && notemp == 'Y') {
			return('Y');
		}
		if (extarr && extarr != '') {
			let i = 0;
			while (extarr[i]) {
				if (tempstring.indexOf('.' + extarr[i]) != -1) {
					return('Y');
				}
				i++;
			}
		}
		return('N');
}

// Check whether the url includes a substring on the suspect list
function checksusurl(url) {
	var urlreq = url.toLowerCase();
	let i = 0;
	while (susURLs[i]) {
		if (urlreq.indexOf(susURLs[i]) != -1) {
			return "Y";
		}
		i++;
	}
	return("N");
}

module.exports = {checkip,checkbot,buildline,buildcols,getip,checkinclude,checkexclude,checksusurl};